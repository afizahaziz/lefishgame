import React, { useState, useEffect } from "react";

function Dots() {
  const [dots, setDots] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      // eslint-disable-next-line no-shadow
      setDots((dots) => (dots + 1) % 4);
    }, 500);
    return () => clearInterval(interval);
  }, []);

  return <div className="text-[8rem] text-gray-600">{".".repeat(dots)}</div>;
}

export default Dots;
