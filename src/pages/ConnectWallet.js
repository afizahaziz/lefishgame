import React from "react";

import vid from "../assets/video/fishBG.mp4";

export default function ConnectWallet() {
  return (
    <div className="relative h-screen">
      <video
        type="video"
        className="absolute inset-0 object-cover w-full h-full -z-10"
        autoPlay
        loop
        muted
      >
        <source src={vid} type="video/mp4" />
        <track
          src="captions_en.vtt"
          kind="captions"
          srcLang="en"
          label="english_captions"
        />
      </video>
      <div className="Press text-white flex justify-center items-center flex-col h-full">
        <h1 className="blink text-[8rem] text-shadow">Le Fish</h1>
        <a href="/home">
          <button type="button" className="">
            <span className="text-[2rem] hover:underline hover:underline-offset-[1rem]">
              Start
            </span>
          </button>
        </a>
      </div>
    </div>
  );
}
