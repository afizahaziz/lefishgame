import React from "react";

import pic from "../../assets/img/pic.png";

export default function Fish() {
  const fish = [
    {
      image: pic,
      no: "Level 1",
    },
    {
      image: pic,
      no: "Level 2",
    },
    {
      image: pic,
      no: "Level 10",
    },
    {
      image: pic,
      no: "Level 1",
    },
    {
      image: pic,
      no: "Level 7",
    },
    {
      image: pic,
      no: "Level 7",
    },
  ];

  return (
    <div className="h-screen">
      <div className="mx-10">
        <p className="text-[5vh] Sniglet">MY COLLECTION</p>
        <div className="grid grid-flow-row grid-cols-5 mt-8 gap-8 Sniglet pb-10">
          {fish.map((item, index) => (
            <div
              className="bg-[#f8dfbf] rounded-lg border-2 border-black py-2 px-4 transform hover:scale-110 shadow-lg"
              key={index}
            >
              <header className="m-4 bg-transparent flex justify-center items-center">
                <img src={item.image} alt="" className="w-48" />
              </header>
              <footer className="mx-8">
                <div className="bg-black rounded-lg">
                  <div className="flex flex-row justify-center items-center px-6 py-2 text-white">
                    <p className="text-xl">{item.no}</p>
                  </div>
                </div>
              </footer>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
