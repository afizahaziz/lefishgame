import React, { useState, useEffect } from "react";

function FishCard({ level }) {
  return (
    <div className="bg-white rounded-lg p-5 shadow-lg">
      <h3 className="text-lg font-medium">Fish Card</h3>
      <p className="text-gray-700">Level: {level}</p>
    </div>
  );
}

function BattleResult({ result }) {
  return (
    <div
      className={`fixed bottom-0 left-0 right-0 bg-white p-5 rounded-lg shadow-lg text-center ${
        result === "win"
          ? "bg-green-100 text-green-800"
          : "bg-red-100 text-red-800"
      }`}
    >
      <p className="font-medium text-lg">
        {result === "win" ? "You Win!" : "You Lose"}
      </p>
    </div>
  );
}

function FishBattle() {
  const [selectedLevel, setSelectedLevel] = useState(0);
  const [opponentLevel, setOpponentLevel] = useState(0);
  const [loading, setLoading] = useState(false);
  const [showResult, setShowResult] = useState(false);
  const [result, setResult] = useState("");

  useEffect(() => {
    if (selectedLevel === 0) return;
    setLoading(true);
    setTimeout(() => {
      setOpponentLevel(Math.floor(Math.random() * 10) + 1);
      setLoading(false);
      setShowResult(true);
      setResult(selectedLevel > opponentLevel ? "win" : "lose");
    }, 2000);
  }, [selectedLevel]);

  return (
    <div className="flex items-center justify-center h-screen">
      <div className="w-full max-w-xs">
        {loading ? (
          <div className="flex items-center justify-center h-32 bg-gray-300 rounded-lg">
            <p className="text-gray-700 text-lg">Loading...</p>
          </div>
        ) : (
          <>
            <FishCard level={selectedLevel} />
            <FishCard level={opponentLevel} />
          </>
        )}
        {!loading && (
          <div className="mt-5">
            <select
              className="bg-white appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              value={selectedLevel}
              onChange={(e) => setSelectedLevel(parseInt(e.target.value))}
            >
              <option value="0">Choose a Level</option>
              <option value="1">Level 1</option>
              <option value="2">Level 2</option>
              <option value="3">Level 3</option>
              <option value="4">Level 4</option>
              <option value="5">Level 5</option>
              <option value="6">Level 6</option>
              <option value="7">Level 7</option>
              <option value="8">Level 8</option>
              <option value="9">Level 9</option>
              <option value="10">Level 10</option>
            </select>
          </div>
        )}
      </div>
      {showResult && <BattleResult result={result} />}
    </div>
  );
}

export default FishBattle;
