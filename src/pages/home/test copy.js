/* eslint-disable jsx-a11y/label-has-for */
import React, { useState } from "react";

const fishData = [
  { name: "Fish 1", level: 3 },
  { name: "Fish 2", level: 5 },
  { name: "Fish 3", level: 4 },
  { name: "Fish 4", level: 2 },
  { name: "Fish 5", level: 6 },
  { name: "Fish 6", level: 1 },
];

function App() {
  const [selectedFish, setSelectedFish] = useState(null);
  const [combinedFish, setCombinedFish] = useState(null);

  const handleFishSelect = (fish) => {
    setSelectedFish(fish);
  };

  const handleFeed = () => {
    if (selectedFish) {
      setCombinedFish({
        name: `Combined Fish`,
        level: selectedFish.level + 1,
      });
      setSelectedFish(null);
    }
  };

  return (
    <div className="p-10">
      <div className="mb-6">
        <label className="block text-gray-700 font-bold mb-2">
          Select Fish:
        </label>
        <div className="grid grid-cols-3 gap-4">
          {fishData
            .filter(
              (fish) => fish.level <= (selectedFish ? selectedFish.level : 10)
            )
            .map((fish) => (
              <div
                key={fish.name}
                className={`cursor-pointer border border-gray-400 rounded ${
                  selectedFish === fish ? "bg-gray-200" : "hover:bg-gray-300"
                } p-2`}
                onClick={() => handleFishSelect(fish)}
              >
                {fish.name} - Level {fish.level}
              </div>
            ))}
        </div>
      </div>
      {selectedFish && (
        <div className="mb-6">
          <button
            type="button"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            onClick={handleFeed}
          >
            Feed
          </button>
        </div>
      )}
      {combinedFish && (
        <div className="mb-6 bg-green-200 p-4 rounded">
          <p className="font-bold">
            Combined Fish: {combinedFish.name} - Level {combinedFish.level}
          </p>
        </div>
      )}
    </div>
  );
}

export default App;
