/* eslint-disable no-nested-ternary */
import React, { useState } from "react";

import pic from "../../assets/img/pic.png";

function BattleResult({ show, result, onClose }) {
  if (!show) {
    return null;
  }

  return (
    <div className="fixed top-0 left-0 right-0 bottom-0 bg-gray-300 bg-opacity-80 rounded-lg text-center flex items-center justify-center">
      <div
        className={` rounded-lg shadow-lg text-center px-10 py-4 ${
          result === "Win"
            ? "bg-green-100 text-green-800"
            : result === "Lose"
            ? "bg-red-100 text-red-800"
            : "bg-yellow-100 text-yellow-800"
        }`}
      >
        <p className="font-medium text-2xl">
          {result === "Win"
            ? "You Win!"
            : result === "Lose"
            ? "You Lose!"
            : "Draw"}
        </p>

        <button
          type="button"
          className="text-black mt-4 py-2 px-4 border border-black bg-gray-500 rounded-md text-sm hover:bg-gray-300"
          onClick={onClose}
        >
          OK
        </button>
      </div>
    </div>
  );
}

export default function Battle() {
  const [selectedLevel, setSelectedLevel] = useState(0);
  const [opponentLevel, setOpponentLevel] = useState(0);
  const [loading, setLoading] = useState(false);
  const [showResult, setShowResult] = useState(false);
  const [result, setResult] = useState("");

  const handleBattle = () => {
    setLoading(true);
    setTimeout(() => {
      setOpponentLevel(Math.floor(Math.random() * 10) + 1);
      setLoading(false);
      setShowResult(true);

      if (selectedLevel > opponentLevel) {
        setResult("Win");
      } else if (selectedLevel < opponentLevel) {
        setResult("Lose");
      } else if (selectedLevel === opponentLevel) {
        setResult("Draw");
      }
      // setResult(selectedLevel > opponentLevel ? "win" : "lose");
    }, 2000);
  };

  return (
    <div className="Sniglet mb-10">
      <div className="flex flex-col justify-between items-center ">
        <div className="flex flex-row gap-8 mb-5">
          <select
            className="bg-white appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            value={selectedLevel}
            onChange={(e) => setSelectedLevel(parseInt(e.target.value))}
          >
            <option value="0">Choose a Fish</option>
            <option value="1">FISH - Level 1</option>
            <option value="2">FISH - Level 2</option>
            <option value="3">FISH - Level 3</option>
            <option value="4">FISH - Level 4</option>
            <option value="5">FISH - Level 5</option>
            <option value="6">FISH - Level 6</option>
            <option value="7">FISH - Level 7</option>
            <option value="8">FISH - Level 8</option>
            <option value="9">FISH - Level 9</option>
            <option value="10">FISH - Level 10</option>
          </select>
          <button
            type="button"
            className="bg-blue-500 hover:bg-blue-700 text-white font-medium py-2 px-4 rounded-lg"
            disabled={selectedLevel === 0}
            onClick={handleBattle}
          >
            Battle
          </button>
        </div>

        <div className="flex gap-8">
          {loading && (
            <div className="fixed top-0 left-0 right-0 bottom-0 bg-gray-300 bg-opacity-80 rounded-lg text-center flex items-center justify-center">
              <p className="text-gray-700 text-lg">Loading...</p>
            </div>
          )}
          <div className="bg-[#f8dfbf] rounded-lg border-2 border-black py-8 px-4 shadow-lg">
            <header className="m-4 bg-transparent">
              <img src={pic} alt="" className="w-[20rem]" />
            </header>
            <footer className="mx-8">
              <div className="bg-black rounded-lg">
                <div className="flex flex-row justify-center items-center px-6 py-2 text-white">
                  <p className="text-xl">Level {selectedLevel}</p>
                </div>
              </div>
            </footer>
          </div>
          <div className="bg-[#51bcee] rounded-lg border-2 border-black py-8 px-4 shadow-lg">
            <header className="m-4 bg-transparent">
              <img src={pic} alt="" className="w-[20rem]" />
            </header>
            <footer className="mx-8">
              <div className="bg-white rounded-lg">
                <div className="flex flex-row justify-center items-center px-6 py-2 text-black">
                  <p className="text-xl">Level {opponentLevel}</p>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
      <BattleResult
        show={showResult}
        result={result}
        onClose={() => setShowResult(false)}
      />
    </div>
  );
}
