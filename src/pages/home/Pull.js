import React, { useState } from "react";

import gacha from "../../assets/img/capsule.gif";
import stillgacha from "../../assets/img/stillcapsule.png";

function Pull() {
  const [counter, setCounter] = useState(0);
  const [animating, setAnimating] = useState(false);
  const [showModal, setShowModal] = useState(false);

  // function to stop animation after a certain amount of time
  const stopAnimation = (time) => {
    setTimeout(() => {
      setAnimating(false);
      setShowModal(true);
    }, time * 1900);
  };

  // function to handle pull button click
  const handlePullClick = () => {
    setAnimating(true);
    stopAnimation(counter);
  };

  function handleChange(event) {
    if (!isNaN(event.target.value)) setCounter(event.target.value);
  }

  // increase counter
  const increase = () => {
    setCounter(counter + 1);
  };

  // decrease counter
  const decrease = () => {
    if (counter > 0) {
      setCounter((count) => count - 1);
    }
  };

  // reset counter
  const reset = () => {
    setCounter(0);
  };

  return (
    <div className="Sniglet h-full w-full my-10">
      <div className="relative flex justify-center items-center">
        <img src={animating ? gacha : stillgacha} alt="Gacha" />
        <button
          type="button"
          onClick={handlePullClick}
          className="absolute ml-[25rem] py-2 px-3 border-2 border-black bg-green-500 rounded-md text-xl hover:bg-green-300"
        >
          PULL
        </button>
      </div>
      {showModal && (
        <div className="fixed inset-0 h-full w-full bg-gray-700 opacity-95 flex items-center justify-center">
          <div className="bg-white p-8 rounded-md text-center">
            <p className="text-xl font-bold">Complete!</p>
            <button
              type="button"
              className="mt-4 py-2 px-4 border border-black bg-gray-500 rounded-md text-xl hover:bg-gray-300"
              onClick={() => setShowModal(false)}
            >
              OK
            </button>
          </div>
        </div>
      )}
      <div className="flex flex-col justify-center">
        <div className="text-3xl flex justify-center items-center gap-8">
          <button
            type="button"
            className="rounded-md px-4 py-2 border border-black bg-gray-300 hover:bg-gray-500"
            onClick={decrease}
          >
            -
          </button>
          <input
            type="number"
            value={counter}
            onChange={handleChange}
            className="appearance-none w-24 text-center py-2"
          />
          <button
            type="button"
            className="rounded-md px-4 py-2 border border-black bg-gray-300 hover:bg-gray-500"
            onClick={increase}
          >
            +
          </button>
        </div>
        <button
          type="button"
          className="flex justify-center mx-auto py-2 px-3 border-2 border-black bg-gray-500 rounded-md text-xl hover:bg-gray-300 mt-5"
          onClick={reset}
        >
          Reset
        </button>
      </div>
    </div>
  );
}

export default Pull;
