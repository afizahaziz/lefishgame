import React from "react";
import { Tab } from "@headlessui/react";
// import { BsArrowDownShort } from "react-icons/bs";
import Fish from "./Fish";
import Feed from "./Feed";
import Battle from "./Battle";
import Pull from "./Pull";
import fish from "../../assets/img/fish2.gif";
//  bg-cover from-[#0a161e] to-[#4a6c78] bg-gradient-to-b
export default function Index() {
  return (
    <div className="h-full">
      <div className=" mx-10 shadow-md relative pt-4 rounded-md">
        <img src={fish} alt="" className="rounded-md object-cover w-full" />
        <div className="absolute text-center inset-0 flex flex-col items-center justify-center text-white">
          <h1 className="text-[5rem] text-shadow Press ">Le Fish</h1>
          <p className="Sniglet text-[2rem]">Something fishy is going on.</p>
        </div>
      </div>
      {/* <div className="flex justify-end mx-10 my-5">
        <button
          type="button"
          className="Sniglet text-black bg-white py-2 px-4 rounded-full border-2 border-black hover:bg-black hover:text-white hover:border-white"
        >
          <span className="flex gap-1 justify-center items-center text-base">
            sort by <BsArrowDownShort />
          </span>
        </button>
      </div> */}
      <div className="flex justify-center flex-col items-center Sniglet pt-8">
        <Tab.Group>
          <Tab.List className="flex justify-evenly w-full text-[2rem] text-[#3C2A21]">
            <Tab className="hover:underline underline-offset-[2rem] hover:text-[#D5CEA3] hover:decoration-[#1A120B] focus:outline-none focus:text-[#D5CEA3]">
              Fish
            </Tab>
            <Tab className="hover:underline underline-offset-[2rem] hover:text-[#D5CEA3] hover:decoration-[#1A120B] focus:outline-none focus:text-[#D5CEA3]">
              Feed
            </Tab>
            <Tab className="hover:underline underline-offset-[2rem] hover:text-[#D5CEA3] hover:decoration-[#1A120B] focus:outline-none focus:text-[#D5CEA3]">
              Battle
            </Tab>
            <Tab className="hover:underline underline-offset-[2rem] hover:text-[#D5CEA3] hover:decoration-[#1A120B] focus:outline-none focus:text-[#D5CEA3]">
              Pull
            </Tab>
          </Tab.List>
          <Tab.Panels className="mt-10 flex justify-start">
            <Tab.Panel>
              <Fish />
            </Tab.Panel>
            <Tab.Panel>
              <Feed />
            </Tab.Panel>
            <Tab.Panel>
              <Battle />
            </Tab.Panel>
            <Tab.Panel>
              <Pull />
            </Tab.Panel>
          </Tab.Panels>
        </Tab.Group>
      </div>
    </div>
  );
}
