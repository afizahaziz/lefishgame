/* eslint-disable no-use-before-define */
import React, { useState } from "react";

function FishCard({ name, level, onClick }) {
  return (
    <div
      className="w-64 h-64 bg-white p-4 m-4 rounded-lg shadow-lg cursor-pointer border-2 border-black focus:border-red-400"
      onClick={onClick}
    >
      <h2 className="text-lg font-bold">{name}</h2>
      <p className="text-sm">Level: {level}</p>
    </div>
  );
}

function NewFishCard(newCard) {
  return (
    <div className="w-64 h-64 bg-white p-4 m-4 rounded-lg shadow-lg cursor-pointer border-2 border-black">
      <h2 className="text-lg font-bold">{newCard.name}</h2>
      <p className="text-sm">{newCard.level}</p>
    </div>
  );
}

function App() {
  const [selectedCards, setSelectedCards] = useState([]);
  const [error, setError] = useState(null);

  const handleCardClick = (card) => {
    if (selectedCards.length < 2) {
      setSelectedCards([...selectedCards, card]);
    } else {
      setError("You can only select two cards at a time");
    }
  };

  const handleFeed = () => {
    if (selectedCards.length === 2) {
      const [firstCard, secondCard] = selectedCards;
      if (firstCard.level > secondCard.level) {
        const newCard = {
          name: `${firstCard.name} + ${secondCard.name}`,
          level: firstCard.level + secondCard.level,
        };
        setSelectedCards([]);
        setError(null);
        // console.log(newCard);
        NewFishCard(newCard);
      } else {
        setError("The level of the second card must be lower than the first");
      }
    } else {
      setError("Please select two cards");
    }
  };

  return (
    <div className="flex justify-center">
      <div className="w-full">
        <h1 className="text-center font-bold mb-4">Fish Card Collection</h1>
        <div className="flex justify-center ">
          {fishCards.map((fishCard) => (
            <FishCard
              key={fishCard.id}
              name={fishCard.name}
              level={fishCard.level}
              onClick={() => handleCardClick(fishCard)}
            />
          ))}
        </div>
        {error && <p className="text-red-500 text-center mt-4">{error}</p>}
        <button
          type="button"
          className="bg-blue-500 text-white p-2 mt-4 rounded-lg shadow-lg"
          onClick={handleFeed}
        >
          Feed
        </button>
      </div>
    </div>
  );
}

const fishCards = [
  { id: 1, name: "Goldfish", level: 1 },
  { id: 2, name: "Angelfish", level: 2 },
  { id: 3, name: "Shark", level: 3 },
  { id: 4, name: "Koi", level: 4 },
];

export default App;
