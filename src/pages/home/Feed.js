import React from "react";

import pic from "../../assets/img/pic.png";

export default function Feed() {
  const fish = [
    {
      image: pic,
      no: "Level 1",
    },
    {
      image: pic,
      no: "Level 2",
    },
    {
      image: pic,
      no: "Level 10",
    },
    {
      image: pic,
      no: "Level 1",
    },
    {
      image: pic,
      no: "Level 7",
    },
    {
      image: pic,
      no: "Level 7",
    },
  ];

  return (
    <div className="Press mt-5">
      <div className="mx-10">
        <div className="relative flex justify-center items-center mb-10 ">
          <div className="border rounded border-transparent bg-black w-[291px] h-[291px]">
            <img src={pic} alt="" className="shadow-2xl w-[291px] h-[291px]" />
          </div>
          <button
            type="button"
            className="bg-pixelate bg-cover bg-[#A75D5D] opacity-80 px-4 py-2 rounded-md hover:opacity-100 absolute flex justify-center"
          >
            <span className="text-[1.2rem] stroke-black Sniglet">
              Choose Fish
            </span>
          </button>
          <button
            type="button"
            className="bg-pixelate bg-cover bg-[#9E7676] opacity-80 px-4 py-2 rounded-md hover:opacity-100 absolute flex item-center right-[15rem]"
          >
            <span className="text-[2rem] stroke-black Sniglet">Feed</span>
          </button>
        </div>

        <div className="h-screen">
          <div className="mx-10">
            <p className="text-[5vh] Sniglet">MY COLLECTION</p>
            <div className="grid grid-flow-row grid-cols-5 mt-8 gap-8 Sniglet pb-10">
              {fish.map((item, index) => (
                <div
                  className="bg-[#f8dfbf] rounded-lg border-2 border-black py-8 px-4 transform hover:scale-110 shadow-lg"
                  key={index}
                >
                  <header className="m-4 bg-transparent flex justify-center items-center">
                    <img src={item.image} alt="" className="w-48" />
                  </header>
                  <footer className="mx-8">
                    <div className="bg-black rounded-lg">
                      <div className="flex flex-row justify-center items-center px-6 py-2 text-white">
                        <p className="text-xl">{item.no}</p>
                      </div>
                    </div>
                  </footer>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
