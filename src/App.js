import React from "react";
import { Routes, Route } from "react-router-dom";

import "./App.css";
import NotFound from "./pages/error/NotFound";
// import Home from "./pages/home";
import ConnectWallet from "./pages/ConnectWallet";
import Home from "./pages/home/Index";
import Test from "./pages/home/test";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route exact path="/" element={<ConnectWallet />} />
        <Route exact path="/home" element={<Home />} />
        <Route exact path="/test" element={<Test />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
